import React from 'react';
 
const ToDo = ({todo,handleToggle , handleDelete}) => {

    const handleClick=(e)=>{
        e.preventDefault();
        handleToggle(e.target.id);
    }
    const handleDel=(e)=>{
        e.preventDefault();
        handleDelete(e.target.id);
    }
   return (
    <> <div className={'list'}>
       <span id={todo.id} className={todo.complete ? "strike" : ""} onClick={handleClick}>
        {todo.task}
       </span>
       <span>
         <button id={todo.id} onClick={handleDel}>Delete</button>
       </span>
       </div>
    </>
   );
};
 
export default ToDo;