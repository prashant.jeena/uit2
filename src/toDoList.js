import React from 'react';
import ToDo from './ToDo';
 
 
const ToDoList = ({toDoList,handleToggle,handleDelete}) => {
   return (
       <div>
           {toDoList.map(todo => {
               return (
                   <ToDo todo={todo} handleToggle={handleToggle} handleDelete={handleDelete} />
               )
           })}
       </div>
   );
};
 
export default ToDoList;