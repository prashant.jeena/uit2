import './App.css';
import React, { useState } from 'react';
import data from './data.json';
import ToDoList from './toDoList';

function App() {

  const [ toDoList, setToDoList ] = useState(data);

  const handleToggle = (id) => {
    let mapped = toDoList.map(task => {
        return task.id === Number(id) ? { ...task, complete: !task.complete } : { ...task};
      });
    setToDoList(mapped);
  }

  const handleDelete=(id)=>{
    console.log(id)
    let filtered = toDoList.filter(task=> task.id!==Number(id));
    setToDoList(filtered);
  }
  
  const addTask = (userInput ) => {
    let copy = [...toDoList];
    copy = [...copy, { id: toDoList.length + 1, task: userInput, complete: false }];
    setToDoList(copy);
  }
  const [ userInput, setUserInput ] = useState('');

  const handleChange = (e) => {
      setUserInput(e.currentTarget.value)
  }

  const handleSubmit = (e) => {
      e.preventDefault();
      addTask(userInput);
      setUserInput("");
  }

  return (
    <div className="App">
      <div>
      <h1>
        To Do List
      </h1>
      <ToDoList toDoList={toDoList} handleToggle={handleToggle} handleDelete={handleDelete}/>

      </div>
      
      <div>
        
      <form onSubmit={handleSubmit}>
            <input value={userInput} type="text" onChange={handleChange} placeholder="Enter task..."/>
            <button>Submit</button>
        </form>
      </div>
    </div>
  );
}

export default App;
